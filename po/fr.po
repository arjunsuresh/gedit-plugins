# French translation of gedit-plugins.
# Copyright (C) 1998-2016 Free Software Foundation, Inc.
# This file is distributed under the same license as the gedit-plugins package.
#
# Christophe Merlet <redfox@redfoxcenter.org>, 2000-2006.
# Jonathan Ernst <jonathan@ernstfamily.ch>, 2006.
# Stéphane Raimbault <stephane.raimbault@gmail.com>, 2007.
# Robert-André Mauchin <zebob.m@pengzone.org>, 2007.
# Laurent Coudeur <laurentc@iol.ie>, 2009.
# Bruno Brouard <annoa.b@gmail.com>, 2011-12.
# Alain Lojewski <allomervan@gmail.com, 2014.
# Claude Paroz <claude@2xlibre.net>, 2008-2018.
# Charles Monzat <charles.monzat@free.fr>, 2018-2020.
#
msgid ""
msgstr ""
"Project-Id-Version: gedit-plugins HEAD\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gedit-plugins/issues\n"
"POT-Creation-Date: 2020-09-24 15:07+0000\n"
"PO-Revision-Date: 2020-12-24 21:59+0100\n"
"Last-Translator: Charles Monzat <charles.monzat@free.fr>\n"
"Language-Team: GNOME French Team <gnomefr@traduc.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Gtranslator 3.38.0\n"

#: plugins/bookmarks/bookmarks.plugin.desktop.in.in:5
#: plugins/bookmarks/gedit-bookmarks.metainfo.xml.in:6
msgid "Bookmarks"
msgstr "Signets"

# Ajout d’un point final afin d’uniformiser avec les autres descriptions.
#: plugins/bookmarks/bookmarks.plugin.desktop.in.in:6
#: plugins/bookmarks/gedit-bookmarks.metainfo.xml.in:7
msgid "Easy document navigation with bookmarks"
msgstr "Naviguer facilement dans vos documents avec les signets."

#: plugins/bookmarks/gedit-bookmarks-app-activatable.c:141
msgid "Toggle Bookmark"
msgstr "Activer/désactiver le signet"

#: plugins/bookmarks/gedit-bookmarks-app-activatable.c:145
msgid "Go to Next Bookmark"
msgstr "Aller au signet suivant"

#: plugins/bookmarks/gedit-bookmarks-app-activatable.c:149
msgid "Go to Previous Bookmark"
msgstr "Aller au signet précédent"

#: plugins/bracketcompletion/bracketcompletion.plugin.desktop.in.in:6
#: plugins/bracketcompletion/gedit-bracketcompletion.metainfo.xml.in:6
msgid "Bracket Completion"
msgstr "Complétion de parenthèses"

#: plugins/bracketcompletion/bracketcompletion.plugin.desktop.in.in:7
msgid "Automatically adds closing brackets."
msgstr "Ajouter automatiquement les parenthèses fermantes."

#: plugins/bracketcompletion/gedit-bracketcompletion.metainfo.xml.in:7
msgid "Automatically add a closing bracket when you insert one"
msgstr ""
"Ajoute automatiquement une parenthèse fermante après saisie d’une ouvrante"

#: plugins/charmap/charmap/__init__.py:56
#: plugins/charmap/charmap.plugin.desktop.in.in:6
msgid "Character Map"
msgstr "Table de caractères"

#: plugins/charmap/charmap.plugin.desktop.in.in:7
msgid "Insert special characters just by clicking on them."
msgstr "Insérer des caractères spéciaux juste en cliquant dessus."

#: plugins/charmap/gedit-charmap.metainfo.xml.in:6
msgid "Charmap"
msgstr "Table de caractères"

#: plugins/charmap/gedit-charmap.metainfo.xml.in:7
msgid "Select characters from a character map"
msgstr "Sélectionne des caractères depuis une table"

#: plugins/codecomment/codecomment.plugin.desktop.in.in:6
#: plugins/codecomment/gedit-codecomment.metainfo.xml.in:6
msgid "Code Comment"
msgstr "Commentateur de code"

#: plugins/codecomment/codecomment.plugin.desktop.in.in:7
msgid "Comment out or uncomment a selected block of code."
msgstr "Commenter ou décommenter un bloc de code sélectionné."

#: plugins/codecomment/codecomment.py:118
msgid "Co_mment Code"
msgstr "_Commenter le code"

#: plugins/codecomment/codecomment.py:124
msgid "U_ncomment Code"
msgstr "_Décommenter le code"

#: plugins/codecomment/gedit-codecomment.metainfo.xml.in:7
msgid "Comment or uncomment blocks of code"
msgstr "Commente ou décommente des blocs de code"

#: plugins/colorpicker/colorpicker.plugin.desktop.in.in:6
msgid "Color Picker"
msgstr "Sélecteur de couleurs"

#: plugins/colorpicker/colorpicker.plugin.desktop.in.in:7
msgid "Pick a color from a dialog and insert its hexadecimal representation."
msgstr ""
"Choisir une couleur dans une boîte de dialogue et l’insérer sous sa forme "
"hexadécimale."

#: plugins/colorpicker/colorpicker.py:132
msgid "Pick _Color…"
msgstr "Sélectionner une _couleur…"

#: plugins/colorpicker/colorpicker.py:172
msgid "Pick Color"
msgstr "Sélectionner une couleur"

#: plugins/colorpicker/gedit-colorpicker.metainfo.xml.in:6
msgid "Color picker"
msgstr "Sélecteur de couleurs"

#: plugins/colorpicker/gedit-colorpicker.metainfo.xml.in:7
msgid "Select and insert a color from a dialog (for HTML, CSS, PHP)"
msgstr ""
"Sélectionne et applique une couleur d’une boîte de dialogue (pour code HTML, "
"CSS, PHP)"

#: plugins/colorschemer/colorschemer.plugin.desktop.in.in:6
#: plugins/colorschemer/gedit-colorschemer.metainfo.xml.in:6
#: plugins/colorschemer/schemer/__init__.py:50
#: plugins/colorschemer/schemer.ui:20
msgid "Color Scheme Editor"
msgstr "Éditeur de jeu de couleurs"

# Ajout d’un point final afin d’uniformiser avec les autres descriptions.
#: plugins/colorschemer/colorschemer.plugin.desktop.in.in:7
msgid "Source code color scheme editor"
msgstr "Éditeur de jeu de couleurs pour le code source."

#: plugins/colorschemer/gedit-colorschemer.metainfo.xml.in:7
msgid "Create and edit the color scheme used for syntax highlighting"
msgstr "Crée et modifie la couleur du thème servant à surligner la syntaxe"

#: plugins/colorschemer/schemer/schemer.py:271
#: plugins/colorschemer/schemer/schemer.py:318
msgid "There was a problem saving the scheme"
msgstr "Un problème est survenu lors de l’enregistrement du jeu"

#: plugins/colorschemer/schemer/schemer.py:272
msgid ""
"You have chosen to create a new scheme\n"
"but the Name or ID you are using is being used already.\n"
"\n"
"Please make sure to choose a Name and ID that are not already in use.\n"
msgstr ""
"Vous avez choisi de créer un nouveau jeu\n"
"mais le nom ou l’identifiant que vous souhaitez utiliser est déjà attribué.\n"
"\n"
"Veuillez vous assurer de choisir un nom et un identifiant qui ne soient pas "
"déjà utilisés.\n"

#: plugins/colorschemer/schemer/schemer.py:319
msgid ""
"You do not have permission to overwrite the scheme you have chosen.\n"
"Instead a copy will be created.\n"
"\n"
"Please make sure to choose a Name and ID that are not already in use.\n"
msgstr ""
"Vous n’êtes pas autorisé à remplacer le jeu que vous avez sélectionné.\n"
"Au lieu de cela, une copie sera créée.\n"
"\n"
"Veuillez vous assurer de choisir un nom et un identifiant qui ne soient pas "
"déjà utilisés.\n"

#. there must have been some conflict, since it opened the wrong file
#: plugins/colorschemer/schemer/schemer.py:378
msgid "There was a problem opening the file"
msgstr "Un problème est survenu lors de l’ouverture du fichier"

#: plugins/colorschemer/schemer/schemer.py:379
msgid "You appear to have schemes with the same IDs in different directories\n"
msgstr ""
"Il semble que vous possédez des jeux avec des identifiants identiques, "
"situés dans des répertoires différents\n"

#: plugins/colorschemer/schemer.ui:137 plugins/colorschemer/schemer.ui:138
msgid "Bold"
msgstr "Gras"

#: plugins/colorschemer/schemer.ui:160 plugins/colorschemer/schemer.ui:161
msgid "Italic"
msgstr "Italique"

#: plugins/colorschemer/schemer.ui:183 plugins/colorschemer/schemer.ui:184
msgid "Underline"
msgstr "Souligné"

#: plugins/colorschemer/schemer.ui:206 plugins/colorschemer/schemer.ui:207
msgid "Strikethrough"
msgstr "Barré"

#: plugins/colorschemer/schemer.ui:243
msgid "Pick the background color"
msgstr "Choisir la couleur d’arrière-plan"

#: plugins/colorschemer/schemer.ui:259
msgid "Pick the foreground color"
msgstr "Choisir la couleur du premier plan"

#: plugins/colorschemer/schemer.ui:270
msgid "_Background"
msgstr "A_rrière-plan"

#: plugins/colorschemer/schemer.ui:288
msgid "_Foreground"
msgstr "_Premier plan"

#: plugins/colorschemer/schemer.ui:341
#| msgid "_Clear "
msgid "_Clear"
msgstr "Effa_cer"

#: plugins/colorschemer/schemer.ui:383
msgid "Name"
msgstr "Nom"

#: plugins/colorschemer/schemer.ui:408
msgid "ID"
msgstr "Identifiant"

#: plugins/colorschemer/schemer.ui:433
msgid "Description"
msgstr "Description"

#: plugins/colorschemer/schemer.ui:459
msgid "Author"
msgstr "Auteur"

#: plugins/colorschemer/schemer.ui:503
msgid "Sample"
msgstr "Exemple"

#: plugins/commander/commander/appactivatable.py:56
msgid "Commander Mode"
msgstr "Mode ligne de commande"

#: plugins/commander/commander.plugin.desktop.in.in:6
#: plugins/commander/gedit-commander.metainfo.xml.in:6
msgid "Commander"
msgstr "Ligne de commande"

# Ajout d’un point final afin d’uniformiser avec les autres descriptions.
#: plugins/commander/commander.plugin.desktop.in.in:7
#: plugins/commander/gedit-commander.metainfo.xml.in:7
msgid "Command line interface for advanced editing"
msgstr "Interface en ligne de commande pour l’édition avancée."

#: plugins/drawspaces/drawspaces.plugin.desktop.in.in:5
#: plugins/drawspaces/org.gnome.gedit.plugins.drawspaces.gschema.xml:20
msgid "Draw Spaces"
msgstr "Indicateur d’espaces"

# Ajout d’un point final afin d’uniformiser avec les autres descriptions.
#: plugins/drawspaces/drawspaces.plugin.desktop.in.in:6
msgid "Draw spaces and tabs"
msgstr "Indiquer les espaces et les tabulations."

#: plugins/drawspaces/gedit-drawspaces-app-activatable.c:157
msgid "Show _White Space"
msgstr "Afficher les _espaces vides"

#: plugins/drawspaces/gedit-drawspaces-configurable.ui:57
#: plugins/drawspaces/gedit-drawspaces.metainfo.xml.in:6
msgid "Draw spaces"
msgstr "Indiquer les espaces"

#: plugins/drawspaces/gedit-drawspaces-configurable.ui:73
msgid "Draw tabs"
msgstr "Indiquer les tabulations"

#: plugins/drawspaces/gedit-drawspaces-configurable.ui:89
msgid "Draw new lines"
msgstr "Indiquer les sauts de ligne"

#: plugins/drawspaces/gedit-drawspaces-configurable.ui:104
msgid "Draw non-breaking spaces"
msgstr "Indiquer les espaces insécables"

#: plugins/drawspaces/gedit-drawspaces-configurable.ui:119
msgid "Draw leading spaces"
msgstr "Indiquer les espaces de début"

#: plugins/drawspaces/gedit-drawspaces-configurable.ui:134
msgid "Draw spaces in text"
msgstr "Indiquer les espaces dans le texte"

#: plugins/drawspaces/gedit-drawspaces-configurable.ui:149
msgid "Draw trailing spaces"
msgstr "Indiquer les espaces de fin"

#: plugins/drawspaces/gedit-drawspaces.metainfo.xml.in:7
msgid "Draw Spaces and Tabs"
msgstr "Indique les espaces et les tabulations"

#: plugins/drawspaces/org.gnome.gedit.plugins.drawspaces.gschema.xml:15
#| msgid "Show _White Space"
msgid "Show White Space"
msgstr "Afficher les espaces vides"

#: plugins/drawspaces/org.gnome.gedit.plugins.drawspaces.gschema.xml:16
msgid "If TRUE drawing will be enabled."
msgstr "Si VRAI (TRUE), l’affichage est activé."

#: plugins/drawspaces/org.gnome.gedit.plugins.drawspaces.gschema.xml:21
msgid "The type of spaces to be drawn."
msgstr "Le type d’espace à indiquer."

#: plugins/findinfiles/dialog.ui:7 plugins/findinfiles/dialog.vala:53
#: plugins/findinfiles/findinfiles.plugin.desktop.in.in:5
#: plugins/findinfiles/gedit-findinfiles.metainfo.xml.in:6
msgid "Find in Files"
msgstr "Recherche dans des fichiers"

#: plugins/findinfiles/dialog.ui:23 plugins/findinfiles/dialog.vala:58
#: plugins/findinfiles/result-panel.vala:63
msgid "_Close"
msgstr "_Fermer"

#: plugins/findinfiles/dialog.ui:39
msgctxt "label of the find button"
msgid "_Find"
msgstr "_Chercher"

#: plugins/findinfiles/dialog.ui:72
#| msgctxt "label on the left of the GtkEntry containing text to search"
#| msgid "F_ind "
msgctxt "label on the left of the GtkEntry containing text to search"
msgid "F_ind:"
msgstr "_Chercher :"

#: plugins/findinfiles/dialog.ui:99
msgid "_In:"
msgstr "_Dans :"

#: plugins/findinfiles/dialog.ui:115
msgid "Select a _folder"
msgstr "Sélectionner un d_ossier"

#: plugins/findinfiles/dialog.ui:130
msgid "_Match case"
msgstr "_Respecter la casse"

#: plugins/findinfiles/dialog.ui:146
msgid "Match _entire word only"
msgstr "Mots _entiers uniquement"

#: plugins/findinfiles/dialog.ui:162
msgid "Re_gular expression"
msgstr "Expression ré_gulière"

#: plugins/findinfiles/findinfiles.plugin.desktop.in.in:6
msgid "Find text in all files of a folder."
msgstr "Rechercher du texte dans tous les fichiers d’un dossier."

#: plugins/findinfiles/gedit-findinfiles.metainfo.xml.in:7
msgid "Find text in all files of a folder"
msgstr "Recherche de texte dans tous les fichiers d’un dossier"

#: plugins/findinfiles/plugin.vala:159
msgid "Find in Files…"
msgstr "Rechercher dans des fichiers…"

#: plugins/findinfiles/result-panel.vala:127
msgid "hit"
msgid_plural "hits"
msgstr[0] "occurrence"
msgstr[1] "occurrences"

#: plugins/findinfiles/result-panel.vala:196
msgid "No results found"
msgstr "Aucun résultat trouvé"

#: plugins/findinfiles/result-panel.vala:207
msgid "File"
msgstr "Fichier"

#. The stop button is showed in the bottom-left corner of the TreeView
#: plugins/findinfiles/result-panel.vala:218
msgid "Stop the search"
msgstr "Arrêter la recherche"

#: plugins/git/gedit-git.metainfo.xml.in:6
#: plugins/git/git.plugin.desktop.in.in:6
msgid "Git"
msgstr "Git"

#: plugins/git/gedit-git.metainfo.xml.in:7
msgid ""
"Use git information to display which lines and files changed since last "
"commit"
msgstr ""
"Utilise les informations du dépôt git pour afficher les lignes et les "
"fichiers qui ont été modifiés depuis le dernier commit"

# Ajout d’un point final afin d’uniformiser avec les autres descriptions.
#: plugins/git/git.plugin.desktop.in.in:7
msgid "Highlight lines that have been changed since the last commit"
msgstr "Surligner les lignes qui ont été modifiées depuis le dernier commit."

#: plugins/joinlines/gedit-joinlines.metainfo.xml.in:6
msgid "Join lines/ Split lines"
msgstr "Joindre/couper les lignes"

#: plugins/joinlines/gedit-joinlines.metainfo.xml.in:7
msgid "Join or split multiple lines through Ctrl+J and Ctrl+Shift+J"
msgstr "Joint ou coupe plusieurs lignes avec Ctrl+J et Ctrl+Maj+J"

#: plugins/joinlines/joinlines.plugin.desktop.in.in:6
msgid "Join/Split Lines"
msgstr "Joindre/couper les lignes"

#: plugins/joinlines/joinlines.plugin.desktop.in.in:7
msgid "Join several lines or split long ones"
msgstr "Joindre plusieurs lignes ou séparer les longues lignes"

#: plugins/joinlines/joinlines.py:111
msgid "_Join Lines"
msgstr "_Joindre des lignes"

#: plugins/joinlines/joinlines.py:117
msgid "_Split Lines"
msgstr "_Couper des lignes"

#: plugins/multiedit/gedit-multiedit.metainfo.xml.in:6
msgid "Multi edit"
msgstr "Édition multiple"

# Ajout d’un point final afin d’uniformiser avec les autres descriptions.
#: plugins/multiedit/gedit-multiedit.metainfo.xml.in:7
#: plugins/multiedit/multiedit.plugin.desktop.in.in:7
msgid "Edit document in multiple places at once"
msgstr "Éditer un document à plusieurs endroits en même temps."

#: plugins/multiedit/multiedit/appactivatable.py:44
#: plugins/multiedit/multiedit/viewactivatable.py:1351
msgid "Multi Edit Mode"
msgstr "Mode d’édition multiple"

#: plugins/multiedit/multiedit.plugin.desktop.in.in:6
msgid "Multi Edit"
msgstr "Édition multiple"

#: plugins/multiedit/multiedit/viewactivatable.py:317
msgid "Added edit point…"
msgstr "Point d’édition ajouté…"

#: plugins/multiedit/multiedit/viewactivatable.py:659
msgid "Column Mode…"
msgstr "Mode colonne…"

#: plugins/multiedit/multiedit/viewactivatable.py:777
msgid "Removed edit point…"
msgstr "Point d’édition supprimé…"

#: plugins/multiedit/multiedit/viewactivatable.py:943
msgid "Cancelled column mode…"
msgstr "Mode colonne annulé…"

#: plugins/multiedit/multiedit/viewactivatable.py:1306
msgid "Enter column edit mode using selection"
msgstr "Démarre le mode d’édition en colonne en utilisant la sélection"

#: plugins/multiedit/multiedit/viewactivatable.py:1307
msgid "Enter <b>smart</b> column edit mode using selection"
msgstr ""
"Démarre le mode d’édition en colonne <b>intelligent</b> en utilisant la "
"sélection"

#: plugins/multiedit/multiedit/viewactivatable.py:1308
msgid "<b>Smart</b> column align mode using selection"
msgstr ""
"Mode d’alignement de colonne <b>intelligent</b> en utilisant la sélection"

#: plugins/multiedit/multiedit/viewactivatable.py:1309
msgid "<b>Smart</b> column align mode with additional space using selection"
msgstr ""
"Mode d’alignement de colonne <b>intelligent</b> avec espace supplémentaire "
"en utilisant la sélection"

#: plugins/multiedit/multiedit/viewactivatable.py:1311
msgid "Toggle edit point"
msgstr "Basculer le point d’édition"

#: plugins/multiedit/multiedit/viewactivatable.py:1312
msgid "Add edit point at beginning of line/selection"
msgstr "Ajoute un point d’édition au début de la ligne ou de la sélection"

#: plugins/multiedit/multiedit/viewactivatable.py:1313
msgid "Add edit point at end of line/selection"
msgstr "Ajoute un point d’édition à la fin de la ligne ou de la sélection"

#: plugins/multiedit/multiedit/viewactivatable.py:1314
msgid "Align edit points"
msgstr "Aligner les points d’édition"

#: plugins/multiedit/multiedit/viewactivatable.py:1315
msgid "Align edit points with additional space"
msgstr "Aligner les points d’édition avec espace supplémentaire"

#: plugins/sessionsaver/sessionsaver/appactivatable.py:55
msgid "_Manage Saved Sessions…"
msgstr "_Gérer les sessions enregistrées…"

#: plugins/sessionsaver/sessionsaver/appactivatable.py:58
msgid "_Save Session…"
msgstr "_Enregistrer la session…"

#: plugins/sessionsaver/sessionsaver/appactivatable.py:64
#, python-brace-format
msgid "Recover “{0}” Session"
msgstr "Récupérer la session « {0} »"

#: plugins/sessionsaver/sessionsaver/dialogs.py:153
msgid "Session Name"
msgstr "Nom de la session"

#: plugins/sessionsaver/sessionsaver.plugin.desktop.in.in:6
msgid "Session Saver"
msgstr "Enregistreur de session"

#: plugins/sessionsaver/sessionsaver.plugin.desktop.in.in:7
msgid "Save and restore your working sessions"
msgstr "Enregistrer et restaurer vos sessions de travail."

#: plugins/sessionsaver/sessionsaver/ui/sessionsaver.ui:8
msgid "Save session"
msgstr "Enregistrer la session"

#: plugins/sessionsaver/sessionsaver/ui/sessionsaver.ui:76
msgid "Session name:"
msgstr "Nom de la session :"

#: plugins/sessionsaver/sessionsaver/ui/sessionsaver.ui:120
msgid "Saved Sessions"
msgstr "Sessions enregistrées"

#: plugins/smartspaces/gedit-smartspaces.metainfo.xml.in:6
#: plugins/smartspaces/smartspaces.plugin.desktop.in.in:6
msgid "Smart Spaces"
msgstr "Espaces intelligents"

#: plugins/smartspaces/gedit-smartspaces.metainfo.xml.in:7
msgid "Allow to unindent like if you were using tabs while you’re using spaces"
msgstr "Permet de désindenter avec la barre d’espace comme avec la touche Tab"

#: plugins/smartspaces/smartspaces.plugin.desktop.in.in:7
msgid "Forget you’re not using tabulations."
msgstr "Oubliez que vous n’utilisez pas les tabulations."

#: plugins/synctex/gedit-synctex.metainfo.xml.in:6
#: plugins/synctex/synctex.plugin.desktop.in.in:6
msgid "SyncTeX"
msgstr "SyncTeX"

#: plugins/synctex/gedit-synctex.metainfo.xml.in:7
msgid "Synchronize between LaTeX and PDF with gedit and evince"
msgstr "Synchronise LaTeX et PDF avec gedit et evince"

#: plugins/synctex/synctex.plugin.desktop.in.in:7
msgid "Synchronize between LaTeX and PDF with gedit and evince."
msgstr "Synchroniser entre LaTeX et PDF avec gedit et evince."

#: plugins/synctex/synctex/synctex.py:342
msgid "Forward Search"
msgstr "Rechercher en avant"

#: plugins/terminal/gedit-terminal.metainfo.xml.in:6
#: plugins/terminal/terminal.py:313
msgid "Terminal"
msgstr "Terminal"

# Ajout d’un point final afin d’uniformiser avec les autres descriptions.
#: plugins/terminal/gedit-terminal.metainfo.xml.in:7
msgid "A simple terminal widget accessible from the bottom panel"
msgstr ""
"Un simple élément graphique du terminal accessible dans le panneau inférieur."

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:16
msgid "Whether to silence terminal bell"
msgstr "Indique s’il faut rendre muet le bip du terminal"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:17
msgid ""
"If true, don’t make a noise when applications send the escape sequence for "
"the terminal bell."
msgstr ""
"Si vrai, n’émet pas de son lorsque des applications envoient la séquence "
"d’échappement pour le signal du terminal."

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:24
msgid "Number of lines to keep in scrollback"
msgstr "Nombre de lignes à conserver dans l’historique de défilement"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:25
msgid ""
"Number of scrollback lines to keep around. You can scroll back in the "
"terminal by this number of lines; lines that don’t fit in the scrollback are "
"discarded. If scrollback-unlimited is true, this value is ignored."
msgstr ""
"Nombre de lignes de défilement à conserver. Vous pouvez remonter dans "
"l’historique du défilement de ce nombre de lignes ; les lignes qui dépassent "
"ce nombre sont effacées. Si « scrollback-unlimited » est vrai, cette valeur "
"est ignorée."

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:34
msgid "Whether an unlimited number of lines should be kept in scrollback"
msgstr ""
"Indique si un nombre de lignes illimité doit être conservé dans la mémoire "
"de défilement"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:37
msgid ""
"If true, scrollback lines will never be discarded. The scrollback history is "
"stored on disk temporarily, so this may cause the system to run out of disk "
"space if there is a lot of output to the terminal."
msgstr ""
"Si vrai, les lignes qui ont défilé ne seront jamais supprimées. L’historique "
"du défilement est enregistré temporairement sur le disque, ce qui peut "
"provoquer un dépassement de capacité du disque s’il y a beaucoup d’affichage "
"à l’écran."

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:46
msgid "Whether to scroll to the bottom when a key is pressed"
msgstr ""
"Indique s’il faut se déplacer tout en bas lorsqu’une touche est enfoncée"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:47
msgid "If true, pressing a key jumps the scrollbar to the bottom."
msgstr ""
"Si vrai, un appui sur une touche déplace la barre de défilement tout en bas."

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:53
msgid "Whether to scroll to the bottom when there’s new output"
msgstr "Indique s’il faut se déplacer tout en bas à chaque nouveau contenu"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:54
msgid ""
"If true, whenever there’s new output the terminal will scroll to the bottom."
msgstr ""
"Si vrai, à chaque nouveau contenu apparaissant dans le terminal, le "
"défilement se place tout en bas."

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:61
msgid "Default color of text in the terminal"
msgstr "Couleur par défaut du texte dans le terminal"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:62
msgid ""
"Default color of text in the terminal, as a color specification (can be HTML-"
"style hex digits, or a color name such as “red”)."
msgstr ""
"Couleur par défaut du texte du terminal sous la forme d’un code de couleur "
"(cela peut être des chiffres hexadécimaux tels que ceux utilisés en HTML ou "
"un nom de couleur tel que « red »)."

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:70
msgid "Default color of terminal background"
msgstr "Couleur par défaut de l’arrière-plan du terminal"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:71
msgid ""
"Default color of terminal background, as a color specification (can be HTML-"
"style hex digits, or a color name such as “red”)."
msgstr ""
"Couleur par défaut de l’arrière-plan du terminal sous la forme d’un code de "
"couleur (cela peut être des chiffres hexadécimaux tels que ceux utilisés en "
"HTML ou un nom de couleur tel que « red »)."

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:94
msgid "Palette for terminal applications"
msgstr "Palette pour les applications du terminal"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:95
msgid ""
"Terminals have a 16-color palette that applications inside the terminal can "
"use. This is that palette, in the form of a colon-separated list of color "
"names. Color names should be in hex format e.g. “#FF00FF”"
msgstr ""
"Les terminaux possèdent une palette de 16 couleurs que les applications à "
"l’intérieur du terminal peuvent utiliser. Il s’agit de cette palette sous la "
"forme d’une liste de noms de couleur séparés par des deux-points. Les noms "
"de couleur doivent être au format hexadécimal, par ex. « #FF00FF »"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:104
msgid "Whether to use the colors from the theme for the terminal widget"
msgstr ""
"Indique s’il faut utiliser les couleurs du thème pour les éléments "
"graphiques du terminal"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:107
msgid ""
"If true, the theme color scheme used for text entry boxes will be used for "
"the terminal, instead of colors provided by the user."
msgstr ""
"Si vrai, le jeu de couleurs du thème utilisé pour les boîtes de saisie de "
"texte est utilisé pour le terminal, à la place des couleurs indiquées par "
"l’utilisateur."

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:114
msgid "Whether to blink the cursor"
msgstr "Indique s’il faut faire clignoter le curseur"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:115
msgid ""
"The possible values are “system” to use the global cursor blinking settings, "
"or “on” or “off” to set the mode explicitly."
msgstr ""
"Les valeurs possibles sont « system » pour utiliser les réglages globaux de "
"clignotement du curseur ou « on » ou « off » pour paramétrer le mode "
"explicitement."

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:122
msgid "The cursor appearance"
msgstr "L’apparence du curseur"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:123
msgid ""
"The possible values are “block” to use a block cursor, “ibeam” to use a "
"vertical line cursor, or “underline” to use an underline cursor."
msgstr ""
"Les valeurs possibles sont « block » pour utiliser un curseur bloc, "
"« ibeam » pour utiliser un curseur de barre verticale ou « underline » pour "
"utiliser un curseur souligné."

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:130
msgid "Whether to use the system font"
msgstr "Indique s’il faut utiliser la police système"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:131
msgid ""
"If true, the terminal will use the desktop-global standard font if it’s "
"monospace (and the most similar font it can come up with otherwise)."
msgstr ""
"Si vrai, le terminal utilise la police standard globale du système si elle "
"est à chasse fixe (sinon, la police la plus proche qu’il peut obtenir)."

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:139
msgid "Font"
msgstr "Police"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:140
msgid "A Pango font name. Examples are “Sans 12” or “Monospace Bold 14”."
msgstr ""
"Un nom de police Pango. Exemples : « Sans 12 » ou « Monospace Bold 14 »."

#: plugins/terminal/terminal.plugin.desktop.in.in:6
msgid "Embedded Terminal"
msgstr "Terminal intégré"

#: plugins/terminal/terminal.plugin.desktop.in.in:7
msgid "Embed a terminal in the bottom pane."
msgstr "Intégrer un terminal dans le panneau inférieur."

#: plugins/terminal/terminal.py:334
msgid "C_hange Directory"
msgstr "C_hanger de répertoire"

#: plugins/textsize/gedit-textsize.metainfo.xml.in:6
msgid "Text size"
msgstr "Taille du texte"

# Ajout d’un point final afin d’uniformiser avec les autres descriptions.
#: plugins/textsize/gedit-textsize.metainfo.xml.in:7
#: plugins/textsize/textsize.plugin.desktop.in.in:7
msgid "Easily increase and decrease the text size"
msgstr "Augmenter ou diminuer facilement la taille du texte."

#: plugins/textsize/textsize/__init__.py:53
msgid "_Normal size"
msgstr "Taille _normale"

#: plugins/textsize/textsize/__init__.py:55
msgid "S_maller Text"
msgstr "Texte plus _petit"

#: plugins/textsize/textsize/__init__.py:57
msgid "_Larger Text"
msgstr "Texte plus _grand"

#: plugins/textsize/textsize.plugin.desktop.in.in:6
msgid "Text Size"
msgstr "Taille du texte"

#: plugins/translate/gedit-translate.metainfo.xml.in:5
#: plugins/translate/translate.plugin.desktop.in.in:6
msgid "Translate"
msgstr "Traduire"

# Utilisation de l’infinitif afin d’uniformiser avec les autres descriptions.
#: plugins/translate/gedit-translate.metainfo.xml.in:6
#: plugins/translate/translate.plugin.desktop.in.in:7
msgid "Translates text into different languages"
msgstr "Traduire un texte en différentes langues."

#: plugins/translate/org.gnome.gedit.plugins.translate.gschema.xml:5
msgid "Where translation output is shown"
msgstr "L’emplacement où les résultats de traduction sont affichés"

#: plugins/translate/org.gnome.gedit.plugins.translate.gschema.xml:6
msgid ""
"If true, output of the translation is inserted in the document window if not "
"in the Output Translate Window."
msgstr ""
"Si vrai, les résultats de traduction sont insérés dans la fenêtre du "
"document, sinon dans la fenêtre des résultats de traduction."

#. Translators: You can adjust the default pair for users in your locale.
#. https://wiki.apertium.org/wiki/List_of_language_pairs lists valid pairs, in
#. the format apertium-xxx-yyy. For this translation, use ASCII apostrophes and
#. | as the delimiter. Language pair values must be in the format 'xxx|yyy' -
#. You must keep this format 'xxx|yyy', or things will break!
#: plugins/translate/org.gnome.gedit.plugins.translate.gschema.xml:16
msgid "'eng|spa'"
msgstr "'eng|fra'"

#: plugins/translate/org.gnome.gedit.plugins.translate.gschema.xml:17
msgid "Language pair used"
msgstr "Paire de langues utilisée"

#: plugins/translate/org.gnome.gedit.plugins.translate.gschema.xml:18
msgid "Language pair used to translate from one language to another"
msgstr "Paire de langues utilisée pour traduire d’une langue à une autre"

#: plugins/translate/org.gnome.gedit.plugins.translate.gschema.xml:24
#: plugins/translate/org.gnome.gedit.plugins.translate.gschema.xml:25
msgid "API key for remote web service"
msgstr "Clé API pour le service Web distant"

#: plugins/translate/org.gnome.gedit.plugins.translate.gschema.xml:31
#: plugins/translate/org.gnome.gedit.plugins.translate.gschema.xml:32
msgid "Remote web service to use"
msgstr "Service Web distant à utiliser"

#: plugins/translate/translate/__init__.py:72
#, python-brace-format
msgid "Translations powered by {0}"
msgstr "Traductions réalisées avec {0}"

#: plugins/translate/translate/__init__.py:75
msgid "Translate Console"
msgstr "Console de traduction"

#: plugins/translate/translate/__init__.py:157
#, python-brace-format
msgid "Translate selected text [{0}]"
msgstr "Traduire le texte sélectionné [{0}]"

#: plugins/translate/translate/preferences.py:84
msgid "API Key"
msgstr "Clé API"

#: plugins/translate/translate/services/yandex.py:65
msgid ""
"You need to obtain a free API key at <a href='https://tech.yandex.com/"
"translate/'>https://tech.yandex.com/translate/</a>"
msgstr ""
"Vous devez obtenir une clé API gratuite sur <a href='https://tech.yandex.com/"
"translate/'>https://tech.yandex.com/translate/</a>"

#: plugins/translate/translate/ui/preferences.ui:23
msgid "Translation languages:"
msgstr "Langues de traduction :"

#: plugins/translate/translate/ui/preferences.ui:60
msgid "Where to output translation:"
msgstr "Destination des résultats de traduction :"

#: plugins/translate/translate/ui/preferences.ui:75
msgid "Same document window"
msgstr "Même fenêtre que le document"

#: plugins/translate/translate/ui/preferences.ui:90
msgid "Translate console (bottom panel)"
msgstr "Console de traduction (panneau inférieur)"

#: plugins/translate/translate/ui/preferences.ui:157
msgid "Translation service:"
msgstr "Service de traduction :"

#: plugins/wordcompletion/gedit-word-completion-configure.ui:18
msgid "Interactive completion"
msgstr "Complétion interactive"

#: plugins/wordcompletion/gedit-word-completion-configure.ui:43
msgid "Minimum word size:"
msgstr "Taille minimum du mot :"

#: plugins/wordcompletion/gedit-wordcompletion.metainfo.xml.in:6
msgid "Word completion"
msgstr "Complétion de mots"

#: plugins/wordcompletion/gedit-wordcompletion.metainfo.xml.in:7
msgid ""
"Propose automatic completion using words already present in the document"
msgstr ""
"Propose une complétion automatique avec des mots déjà présents dans le "
"document"

#: plugins/wordcompletion/gedit-word-completion-plugin.c:181
msgid "Document Words"
msgstr "Mots du document"

#: plugins/wordcompletion/org.gnome.gedit.plugins.wordcompletion.gschema.xml:5
msgid "Interactive Completion"
msgstr "Complétion Interactive"

#: plugins/wordcompletion/org.gnome.gedit.plugins.wordcompletion.gschema.xml:6
msgid "Whether to enable interactive completion."
msgstr "Indique s’il faut activer la complétion interactive."

#: plugins/wordcompletion/org.gnome.gedit.plugins.wordcompletion.gschema.xml:11
msgid "Minimum Word Size"
msgstr "Taille minimum du mot"

#: plugins/wordcompletion/org.gnome.gedit.plugins.wordcompletion.gschema.xml:12
msgid "The minimum word size to complete."
msgstr "Indique la taille minimum du mot à compléter."

#: plugins/wordcompletion/wordcompletion.plugin.desktop.in.in:5
msgid "Word Completion"
msgstr "Complétion de mots"

# Ajout d’un point final afin d’uniformiser avec les autres descriptions.
#: plugins/wordcompletion/wordcompletion.plugin.desktop.in.in:6
msgid "Word completion using the completion framework"
msgstr "Complétion de mots, utilise la structure de complétion."

#~ msgid "_In "
#~ msgstr "_Dans "

#~ msgid "Whether to allow bold text"
#~ msgstr "Indique si un texte gras est autorisé"

#~ msgid "If true, allow applications in the terminal to make text boldface."
#~ msgstr ""
#~ "Si vrai, permet aux applications dans le terminal de mettre le texte en "
#~ "gras."

#~ msgid "Zeitgeist Data provider"
#~ msgstr "Fournisseur de données Zeitgeist"

#~ msgid ""
#~ "Records user activity and giving easy access to recently-used and "
#~ "frequently-used files"
#~ msgstr ""
#~ "Enregistre l’activité de l’utilisateur et permet un accès aisé aux "
#~ "fichiers les plus récemment/fréquemment utilisés"

#~ msgid "Zeitgeist dataprovider"
#~ msgstr "Fournisseur de données Zeitgeist"

#~ msgid "Logs access and leave event for documents used with gedit"
#~ msgstr ""
#~ "Journalise l’accès et laisse l’évènement des documents utilisés avec gedit"

#~ msgid "Empty Document"
#~ msgstr "Document vide"

#~ msgid "Type here to search…"
#~ msgstr "Saisissez ici pour rechercher…"

#~ msgid "Dashboard"
#~ msgstr "Tableau de bord"

#~ msgid "A Dashboard for new tabs"
#~ msgstr "Un tableau de bord pour les nouveaux onglets"

#~ msgid "Displays a grid of recently/most used files upon opening a new tab"
#~ msgstr ""
#~ "Affiche une liste des fichiers les plus récemment/souvent utilisés à "
#~ "l’ouverture d’un nouvel onglet"
